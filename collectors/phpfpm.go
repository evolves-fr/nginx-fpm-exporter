package collectors

import (
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/evolves-fr/nginx-fpm-exporter/pkg/php-fpm"
)

const fpmNamespace = "phpfpm"

func NewFPMCollector(endpoint string, timeout time.Duration, labels []string) prometheus.Collector {
	prometheusLabel := parseLabels(labels)

	return &fpmCollector{
		endpoint: endpoint,
		timeout:  timeout,
		up: prometheus.NewDesc(
			prometheus.BuildFQName(fpmNamespace, "", "up"),
			"The total number of server online.",
			[]string{"endpoint"}, prometheusLabel,
		),
		startSince: prometheus.NewDesc(
			prometheus.BuildFQName(fpmNamespace, "pool", "start_since"),
			"The number of seconds since FPM has started.",
			[]string{"endpoint", "pool", "process_manager"}, prometheusLabel,
		),
		acceptedConn: prometheus.NewDesc(
			prometheus.BuildFQName(fpmNamespace, "pool", "accepted_conn"),
			"The number of request accepted by the pool.",
			[]string{"endpoint", "pool", "process_manager"}, prometheusLabel,
		),
		listenQueue: prometheus.NewDesc(
			prometheus.BuildFQName(fpmNamespace, "pool", "listen_queue"),
			"The number of request in the queue of pending connections.",
			[]string{"endpoint", "pool", "process_manager"}, prometheusLabel,
		),
		maxListenQueue: prometheus.NewDesc(
			prometheus.BuildFQName(fpmNamespace, "pool", "max_listen_queue"),
			"The maximum number of requests in the queue of pending connections since FPM has started.",
			[]string{"endpoint", "pool", "process_manager"}, prometheusLabel,
		),
		listenQueueLen: prometheus.NewDesc(
			prometheus.BuildFQName(fpmNamespace, "pool", "listen_queue_len"),
			"The size of the socket queue of pending connections.",
			[]string{"endpoint", "pool", "process_manager"}, prometheusLabel,
		),
		idleProcesses: prometheus.NewDesc(
			prometheus.BuildFQName(fpmNamespace, "pool", "idle_processes"),
			"The number of idle processes.",
			[]string{"endpoint", "pool", "process_manager"}, prometheusLabel,
		),
		activeProcesses: prometheus.NewDesc(
			prometheus.BuildFQName(fpmNamespace, "pool", "active_processes"),
			"The number of active processes.",
			[]string{"endpoint", "pool", "process_manager"}, prometheusLabel,
		),
		totalProcesses: prometheus.NewDesc(
			prometheus.BuildFQName(fpmNamespace, "pool", "total_processes"),
			"The number of idle + active processes.",
			[]string{"endpoint", "pool", "process_manager"}, prometheusLabel,
		),
		maxActiveProcesses: prometheus.NewDesc(
			prometheus.BuildFQName(fpmNamespace, "pool", "max_active_processes"),
			"The maximum number of active processes since FPM has started.",
			[]string{"endpoint", "pool", "process_manager"}, prometheusLabel,
		),
		maxChildrenReached: prometheus.NewDesc(
			prometheus.BuildFQName(fpmNamespace, "pool", "max_children_reached"),
			"Number of times, the process limit has been reached, when pm tries to start more children.",
			[]string{"endpoint", "pool", "process_manager"}, prometheusLabel,
		),
		slowRequest: prometheus.NewDesc(
			prometheus.BuildFQName(fpmNamespace, "pool", "slow_request"),
			"",
			[]string{"endpoint", "pool", "process_manager"}, prometheusLabel,
		),
	}
}

type fpmCollector struct {
	endpoint           string
	timeout            time.Duration
	up                 *prometheus.Desc
	startSince         *prometheus.Desc
	acceptedConn       *prometheus.Desc
	listenQueue        *prometheus.Desc
	maxListenQueue     *prometheus.Desc
	listenQueueLen     *prometheus.Desc
	idleProcesses      *prometheus.Desc
	activeProcesses    *prometheus.Desc
	totalProcesses     *prometheus.Desc
	maxActiveProcesses *prometheus.Desc
	maxChildrenReached *prometheus.Desc
	slowRequest        *prometheus.Desc
}

func (c fpmCollector) Describe(ch chan<- *prometheus.Desc) {
	ch <- c.up
	ch <- c.startSince
	ch <- c.acceptedConn
	ch <- c.listenQueue
	ch <- c.maxListenQueue
	ch <- c.listenQueueLen
	ch <- c.idleProcesses
	ch <- c.activeProcesses
	ch <- c.totalProcesses
	ch <- c.maxActiveProcesses
	ch <- c.maxChildrenReached
	ch <- c.slowRequest
}

func (c fpmCollector) Collect(ch chan<- prometheus.Metric) {
	var state float64

	if status, err := phpfpm.GetStatus(c.endpoint, c.timeout); err == nil {
		state = 1

		labels := []string{c.endpoint, status.Pool, status.ProcessManager}

		ch <- prometheus.MustNewConstMetric(c.startSince, prometheus.CounterValue, float64(status.StartSince), labels...)
		ch <- prometheus.MustNewConstMetric(c.acceptedConn, prometheus.CounterValue, float64(status.AcceptedConn), labels...)
		ch <- prometheus.MustNewConstMetric(c.listenQueue, prometheus.GaugeValue, float64(status.ListenQueue), labels...)
		ch <- prometheus.MustNewConstMetric(c.maxListenQueue, prometheus.GaugeValue, float64(status.MaxListenQueue), labels...)
		ch <- prometheus.MustNewConstMetric(c.listenQueueLen, prometheus.GaugeValue, float64(status.ListenQueueLen), labels...)
		ch <- prometheus.MustNewConstMetric(c.idleProcesses, prometheus.GaugeValue, float64(status.IdleProcesses), labels...)
		ch <- prometheus.MustNewConstMetric(c.activeProcesses, prometheus.GaugeValue, float64(status.ActiveProcesses), labels...)
		ch <- prometheus.MustNewConstMetric(c.totalProcesses, prometheus.GaugeValue, float64(status.TotalProcesses), labels...)
		ch <- prometheus.MustNewConstMetric(c.maxActiveProcesses, prometheus.GaugeValue, float64(status.MaxActiveProcesses), labels...)
		ch <- prometheus.MustNewConstMetric(c.slowRequest, prometheus.GaugeValue, float64(status.SlowRequests), labels...)

		if status.ProcessManager == phpfpm.ProcessManagerDynamic || status.ProcessManager == phpfpm.ProcessManagerOnDemand {
			ch <- prometheus.MustNewConstMetric(c.maxChildrenReached, prometheus.GaugeValue, float64(status.MaxChildrenReached), labels...)
		}
	}

	ch <- prometheus.MustNewConstMetric(c.up, prometheus.GaugeValue, state, c.endpoint)
}
