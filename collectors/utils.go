package collectors

import (
	"strings"

	"github.com/prometheus/client_golang/prometheus"
)

func parseLabels(labels []string) prometheus.Labels {
	l := make(prometheus.Labels)

	for _, label := range labels {
		split := strings.Split(label, "=")
		if len(split) == 2 {
			l[split[0]] = split[1]
		}
	}

	return l
}
