package collectors

import (
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/evolves-fr/nginx-fpm-exporter/pkg/nginx"
)

const nginxNamespace = "nginx"

func NewNginxCollector(endpoint string, timeout time.Duration, labels []string) prometheus.Collector {
	prometheusLabel := parseLabels(labels)

	return &nginxCollector{
		endpoint: endpoint,
		timeout:  timeout,
		up: prometheus.NewDesc(
			prometheus.BuildFQName(nginxNamespace, "", "up"),
			"The total number of server online.",
			[]string{"endpoint"}, prometheusLabel,
		),
		accepts: prometheus.NewDesc(
			prometheus.BuildFQName(nginxNamespace, "status", "accepts"),
			"The total number of accepted client connections.",
			[]string{"endpoint"}, prometheusLabel,
		),
		connections: prometheus.NewDesc(
			prometheus.BuildFQName(nginxNamespace, "status", "connections"),
			"The current number of active client connections including Waiting connections.",
			[]string{"endpoint"}, prometheusLabel,
		),
		handled: prometheus.NewDesc(
			prometheus.BuildFQName(nginxNamespace, "status", "handled"),
			"The total number of handled connections.",
			[]string{"endpoint"}, prometheusLabel,
		),
		reading: prometheus.NewDesc(
			prometheus.BuildFQName(nginxNamespace, "status", "reading"),
			"The current number of connections where nginx is reading the request header.",
			[]string{"endpoint"}, prometheusLabel,
		),
		requests: prometheus.NewDesc(
			prometheus.BuildFQName(nginxNamespace, "status", "requests"),
			"The total number of client requests.",
			[]string{"endpoint"}, prometheusLabel,
		),
		waiting: prometheus.NewDesc(
			prometheus.BuildFQName(nginxNamespace, "status", "waiting"),
			"The current number of idle client connections waiting for a request.",
			[]string{"endpoint"}, prometheusLabel,
		),
		writing: prometheus.NewDesc(
			prometheus.BuildFQName(nginxNamespace, "status", "writing"),
			"The current number of connections where nginx is writing the response back to the client.",
			[]string{"endpoint"}, prometheusLabel,
		),
	}
}

type nginxCollector struct {
	endpoint    string
	timeout     time.Duration
	accepts     *prometheus.Desc
	connections *prometheus.Desc
	handled     *prometheus.Desc
	reading     *prometheus.Desc
	requests    *prometheus.Desc
	up          *prometheus.Desc
	waiting     *prometheus.Desc
	writing     *prometheus.Desc
}

func (c nginxCollector) Describe(ch chan<- *prometheus.Desc) {
	ch <- c.accepts
	ch <- c.connections
	ch <- c.handled
	ch <- c.reading
	ch <- c.requests
	ch <- c.up
	ch <- c.waiting
	ch <- c.writing
}

func (c nginxCollector) Collect(ch chan<- prometheus.Metric) {
	var state float64

	if status, err := nginx.GetStatus(c.endpoint, c.timeout); err == nil {
		state = 1

		ch <- prometheus.MustNewConstMetric(c.accepts, prometheus.CounterValue, float64(status.Accepts), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.connections, prometheus.GaugeValue, float64(status.ActiveConnections), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.handled, prometheus.CounterValue, float64(status.Handled), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.reading, prometheus.GaugeValue, float64(status.Reading), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.requests, prometheus.CounterValue, float64(status.Requests), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.waiting, prometheus.GaugeValue, float64(status.Waiting), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.writing, prometheus.GaugeValue, float64(status.Writing), c.endpoint)
	}

	ch <- prometheus.MustNewConstMetric(c.up, prometheus.GaugeValue, state, c.endpoint)
}
