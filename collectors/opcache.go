package collectors

import (
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/evolves-fr/nginx-fpm-exporter/pkg/opcache"
)

const (
	opcacheNamespace              = "opcache"
	opcacheConfigurationNamespace = "opcache_configuration"
	opcacheStatusNamespace        = "opcache_status"
)

func NewOPCacheCollector(endpoint string, timeout time.Duration, labels []string, full bool) prometheus.Collector {
	opcacheLabel := parseLabels(labels)

	return &opcacheCollector{
		endpoint: endpoint,
		timeout:  timeout,
		full:     full,
		up: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheNamespace, "", "up"),
			"The total number of server online.",
			[]string{"endpoint"}, opcacheLabel,
		),
		configEnabled: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheConfigurationNamespace, "directives", "enable"),
			"Determines if Zend OPCache is enabled.",
			[]string{"endpoint"}, opcacheLabel,
		),
		configEnabledCli: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheConfigurationNamespace, "directives", "enable_cli"),
			"Determines if Zend OPCache is enabled for the CLI version of PHP.",
			[]string{"endpoint"}, opcacheLabel,
		),
		configUseCWD: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheConfigurationNamespace, "directives", "use_cwd"),
			"When this directive is enabled, the OPcache appends the current working directory to the script key, thus eliminating possible collisions between files with the same name (basename). Disabling the directive improves performance, but may break existing applications.",
			[]string{"endpoint"}, opcacheLabel,
		),
		configValidateTimestamps: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheConfigurationNamespace, "directives", "validate_timestamps"),
			"When disabled, you must reset the OPcache manually or restart the webserver for changes to the filesystem to take effect.",
			[]string{"endpoint"}, opcacheLabel,
		),
		configValidatePermission: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheConfigurationNamespace, "directives", "validate_permission"),
			"Validates the cached file permissions against the current user.",
			[]string{"endpoint"}, opcacheLabel,
		),
		configValidateRoot: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheConfigurationNamespace, "directives", "validate_root"),
			"Prevents name collisions in chroot'ed environments. This should be enabled in all chroot'ed environments to prevent access to files outside the chroot.",
			[]string{"endpoint"}, opcacheLabel,
		),
		configInheritedHack: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheConfigurationNamespace, "directives", "inherited_hack"),
			"",
			[]string{"endpoint"}, opcacheLabel,
		),
		configDupsFix: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheConfigurationNamespace, "directives", "dups_fix"),
			"This hack should only be enabled to work around 'Cannot redeclare class' errors.",
			[]string{"endpoint"}, opcacheLabel,
		),
		configRevalidatePath: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheConfigurationNamespace, "directives", "revalidate_path"),
			"Enables or disables file search in include_path optimization.",
			[]string{"endpoint"}, opcacheLabel,
		),
		configLogVerbosityLevel: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheConfigurationNamespace, "directives", "log_verbosity_level"),
			"All OPcache errors go to the Web server log. By default, only fatal errors (level 0) or errors (level 1) are logged. You can also enable warnings (level 2), info messages (level 3) or debug messages (level 4).",
			[]string{"endpoint"}, opcacheLabel,
		),
		configMemoryConsumption: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheConfigurationNamespace, "directives", "memory_consumption"),
			"The OPcache shared memory storage size.",
			[]string{"endpoint"}, opcacheLabel,
		),
		configInternedStringsBuffer: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheConfigurationNamespace, "directives", "interned_strings_buffer"),
			"The amount of memory for interned strings in Mbytes.",
			[]string{"endpoint"}, opcacheLabel,
		),
		configMaxAcceleratedFiles: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheConfigurationNamespace, "directives", "max_accelerated_files"),
			"The maximum number of keys (scripts) in the OPcache hash table. Only numbers between 200 and 100000 are allowed.",
			[]string{"endpoint"}, opcacheLabel,
		),
		configMaxWastedPercentage: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheConfigurationNamespace, "directives", "max_wasted_percentage"),
			"The maximum percentage of \"wasted\" memory until a restart is scheduled.",
			[]string{"endpoint"}, opcacheLabel,
		),
		configConsistencyChecks: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheConfigurationNamespace, "directives", "consistency_checks"),
			"Check the cache checksum each N requests. The default value of '0' means that the checks are disabled.",
			[]string{"endpoint"}, opcacheLabel,
		),
		configForceRestartTimeout: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheConfigurationNamespace, "directives", "force_restart_timeout"),
			"How long to wait (in seconds) for a scheduled restart to begin if the cache is not being accessed.",
			[]string{"endpoint"}, opcacheLabel,
		),
		configRevalidateFreq: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheConfigurationNamespace, "directives", "revalidate_freq"),
			"How often (in seconds) to check file timestamps for changes to the shared memory storage allocation. '1' means validate once per second, but only once per request. '0' means always validate.",
			[]string{"endpoint"}, opcacheLabel,
		),
		configMaxFileSize: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheConfigurationNamespace, "directives", "max_file_size"),
			"Allows exclusion of large files from being cached. By default all files are cached.",
			[]string{"endpoint"}, opcacheLabel,
		),
		configProtectMemory: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheConfigurationNamespace, "directives", "protect_memory"),
			"Protect the shared memory from unexpected writing during script execution. Useful for internal debugging only.",
			[]string{"endpoint"}, opcacheLabel,
		),
		configSaveComments: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheConfigurationNamespace, "directives", "save_comments"),
			"If disabled, all PHPDoc comments are dropped from the code to reduce the size of the optimized code.",
			[]string{"endpoint"}, opcacheLabel,
		),
		configEnableFileOverride: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheConfigurationNamespace, "directives", "enable_file_override"),
			"Allow file existence override (file_exists, etc.) performance feature.",
			[]string{"endpoint"}, opcacheLabel,
		),
		configOptimizationLevel: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheConfigurationNamespace, "directives", "optimization_level"),
			"A bitmask, where each bit enables or disables the appropriate OPcache passes.",
			[]string{"endpoint"}, opcacheLabel,
		),
		configFileCacheOnly: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheConfigurationNamespace, "directives", "file_cache_only"),
			"Enables or disables opcode caching in shared memory.",
			[]string{"endpoint"}, opcacheLabel,
		),
		configFileCacheConsistencyChecks: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheConfigurationNamespace, "directives", "file_cache_consistency_checks"),
			"Enables or disables checksum validation when script loaded from file cache.",
			[]string{"endpoint"}, opcacheLabel,
		),
		configFileUpdateProtection: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheConfigurationNamespace, "directives", "file_update_protection"),
			"Prevents caching files that are less than this number of seconds old. It protects from caching of incompletely updated files. In case all file updates on your site are atomic, you may increase performance by setting it to '0'.",
			[]string{"endpoint"}, opcacheLabel,
		),
		configOptDebugLevel: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheConfigurationNamespace, "directives", "opt_debug_level"),
			"",
			[]string{"endpoint"}, opcacheLabel,
		),
		configHugeCodePages: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheConfigurationNamespace, "directives", "huge_code_pages"),
			"Enables or disables copying of PHP code (text segment) into HUGE PAGES. This should improve performance, but requires appropriate OS configuration.",
			[]string{"endpoint"}, opcacheLabel,
		),
		statusEnabled: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheStatusNamespace, "", "opcache_enabled"),
			"Determines if OPCache status is enabled.",
			[]string{"endpoint"}, opcacheLabel,
		),
		statusCacheFull: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheStatusNamespace, "", "cache_full"),
			"Determines if OPCache is full",
			[]string{"endpoint"}, opcacheLabel,
		),
		statusRestartPending: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheStatusNamespace, "", "restart_pending"),
			"Determines if OPCache restart is pending",
			[]string{"endpoint"}, opcacheLabel,
		),
		statusRestartInProgress: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheStatusNamespace, "", "restart_in_progress"),
			"Determines if OPCache restart is in progress",
			[]string{"endpoint"}, opcacheLabel,
		),
		statusMemoryUsageUsed: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheStatusNamespace, "memory_usage", "used_memory"),
			"",
			[]string{"endpoint"}, opcacheLabel,
		),
		statusMemoryUsageFree: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheStatusNamespace, "memory_usage", "free_memory"),
			"",
			[]string{"endpoint"}, opcacheLabel,
		),
		statusMemoryUsageWasted: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheStatusNamespace, "memory_usage", "wasted_memory"),
			"",
			[]string{"endpoint"}, opcacheLabel,
		),
		statusMemoryUsageWastedPercent: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheStatusNamespace, "memory_usage", "current_wasted_percentage"),
			"",
			[]string{"endpoint"}, opcacheLabel,
		),
		statusInternedUsageBufferSize: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheStatusNamespace, "interned_strings_usage", "buffer_size"),
			"",
			[]string{"endpoint"}, opcacheLabel,
		),
		statusInternedUsageUsed: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheStatusNamespace, "interned_strings_usage", "used_memory"),
			"",
			[]string{"endpoint"}, opcacheLabel,
		),
		statusInternedUsageFree: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheStatusNamespace, "interned_strings_usage", "free_memory"),
			"",
			[]string{"endpoint"}, opcacheLabel,
		),
		statusInternedUsageNumberOfString: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheStatusNamespace, "interned_strings_usage", "number_of_strings"),
			"",
			[]string{"endpoint"}, opcacheLabel,
		),
		statusStatisticsNumCachedScript: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheStatusNamespace, "opcache_statistics", "num_cached_scripts"),
			"",
			[]string{"endpoint"}, opcacheLabel,
		),
		statusStatisticsNumCachedKeys: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheStatusNamespace, "opcache_statistics", "num_cached_keys"),
			"",
			[]string{"endpoint"}, opcacheLabel,
		),
		statusStatisticsMaxCachedKeys: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheStatusNamespace, "opcache_statistics", "max_cached_keys"),
			"",
			[]string{"endpoint"}, opcacheLabel,
		),
		statusStatisticsHits: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheStatusNamespace, "opcache_statistics", "hits"),
			"",
			[]string{"endpoint"}, opcacheLabel,
		),
		statusStatisticsStartTime: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheStatusNamespace, "opcache_statistics", "start_time"),
			"",
			[]string{"endpoint"}, opcacheLabel,
		),
		statusStatisticsLastRestartTime: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheStatusNamespace, "opcache_statistics", "last_restart_time"),
			"",
			[]string{"endpoint"}, opcacheLabel,
		),
		statusStatisticsOOMRestart: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheStatusNamespace, "opcache_statistics", "oom_restarts"),
			"",
			[]string{"endpoint"}, opcacheLabel,
		),
		statusStatisticsHashRestart: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheStatusNamespace, "opcache_statistics", "hash_restarts"),
			"",
			[]string{"endpoint"}, opcacheLabel,
		),
		statusStatisticsManualRestart: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheStatusNamespace, "opcache_statistics", "manual_restarts"),
			"",
			[]string{"endpoint"}, opcacheLabel,
		),
		statusStatisticsMisses: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheStatusNamespace, "opcache_statistics", "misses"),
			"",
			[]string{"endpoint"}, opcacheLabel,
		),
		statusStatisticsBlacklistMisses: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheStatusNamespace, "opcache_statistics", "blacklist_misses"),
			"",
			[]string{"endpoint"}, opcacheLabel,
		),
		statusStatisticsBlacklistMissRatio: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheStatusNamespace, "opcache_statistics", "blacklist_miss_ratio"),
			"",
			[]string{"endpoint"}, opcacheLabel,
		),
		statusStatisticsHitRate: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheStatusNamespace, "opcache_statistics", "opcache_hit_rate"),
			"",
			[]string{"endpoint"}, opcacheLabel,
		),
		statusScriptsHits: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheStatusNamespace, "scripts", "hits"),
			"Hits of cached scripts.",
			[]string{"endpoint", "full_path"}, opcacheLabel,
		),
		statusScriptsMemoryConsumption: prometheus.NewDesc(
			prometheus.BuildFQName(opcacheStatusNamespace, "scripts", "memory_consumption"),
			"Memory consumption of cached scripts.",
			[]string{"endpoint", "full_path"}, opcacheLabel,
		),
	}
}

type opcacheCollector struct {
	endpoint                           string
	timeout                            time.Duration
	full                               bool
	up                                 *prometheus.Desc
	configEnabled                      *prometheus.Desc
	configEnabledCli                   *prometheus.Desc
	configUseCWD                       *prometheus.Desc
	configValidateTimestamps           *prometheus.Desc
	configValidatePermission           *prometheus.Desc
	configValidateRoot                 *prometheus.Desc
	configInheritedHack                *prometheus.Desc
	configDupsFix                      *prometheus.Desc
	configRevalidatePath               *prometheus.Desc
	configLogVerbosityLevel            *prometheus.Desc
	configMemoryConsumption            *prometheus.Desc
	configInternedStringsBuffer        *prometheus.Desc
	configMaxAcceleratedFiles          *prometheus.Desc
	configMaxWastedPercentage          *prometheus.Desc
	configConsistencyChecks            *prometheus.Desc
	configForceRestartTimeout          *prometheus.Desc
	configRevalidateFreq               *prometheus.Desc
	configMaxFileSize                  *prometheus.Desc
	configProtectMemory                *prometheus.Desc
	configSaveComments                 *prometheus.Desc
	configEnableFileOverride           *prometheus.Desc
	configOptimizationLevel            *prometheus.Desc
	configFileCacheOnly                *prometheus.Desc
	configFileCacheConsistencyChecks   *prometheus.Desc
	configFileUpdateProtection         *prometheus.Desc
	configOptDebugLevel                *prometheus.Desc
	configHugeCodePages                *prometheus.Desc
	statusEnabled                      *prometheus.Desc
	statusCacheFull                    *prometheus.Desc
	statusRestartPending               *prometheus.Desc
	statusRestartInProgress            *prometheus.Desc
	statusMemoryUsageUsed              *prometheus.Desc
	statusMemoryUsageFree              *prometheus.Desc
	statusMemoryUsageWasted            *prometheus.Desc
	statusMemoryUsageWastedPercent     *prometheus.Desc
	statusInternedUsageBufferSize      *prometheus.Desc
	statusInternedUsageUsed            *prometheus.Desc
	statusInternedUsageFree            *prometheus.Desc
	statusInternedUsageNumberOfString  *prometheus.Desc
	statusStatisticsNumCachedScript    *prometheus.Desc
	statusStatisticsNumCachedKeys      *prometheus.Desc
	statusStatisticsMaxCachedKeys      *prometheus.Desc
	statusStatisticsHits               *prometheus.Desc
	statusStatisticsStartTime          *prometheus.Desc
	statusStatisticsLastRestartTime    *prometheus.Desc
	statusStatisticsOOMRestart         *prometheus.Desc
	statusStatisticsHashRestart        *prometheus.Desc
	statusStatisticsManualRestart      *prometheus.Desc
	statusStatisticsMisses             *prometheus.Desc
	statusStatisticsBlacklistMisses    *prometheus.Desc
	statusStatisticsBlacklistMissRatio *prometheus.Desc
	statusStatisticsHitRate            *prometheus.Desc
	statusScriptsHits                  *prometheus.Desc
	statusScriptsMemoryConsumption     *prometheus.Desc
}

func (c opcacheCollector) Describe(ch chan<- *prometheus.Desc) {
	ch <- c.up
	ch <- c.configEnabled
	ch <- c.configEnabledCli
	ch <- c.configUseCWD
	ch <- c.configValidateTimestamps
	ch <- c.configValidatePermission
	ch <- c.configValidateRoot
	ch <- c.configInheritedHack
	ch <- c.configDupsFix
	ch <- c.configRevalidatePath
	ch <- c.configLogVerbosityLevel
	ch <- c.configMemoryConsumption
	ch <- c.configInternedStringsBuffer
	ch <- c.configMaxAcceleratedFiles
	ch <- c.configMaxWastedPercentage
	ch <- c.configConsistencyChecks
	ch <- c.configForceRestartTimeout
	ch <- c.configRevalidateFreq
	ch <- c.configMaxFileSize
	ch <- c.configProtectMemory
	ch <- c.configSaveComments
	ch <- c.configEnableFileOverride
	ch <- c.configOptimizationLevel
	ch <- c.configFileCacheOnly
	ch <- c.configFileCacheConsistencyChecks
	ch <- c.configFileUpdateProtection
	ch <- c.configOptDebugLevel
	ch <- c.configHugeCodePages
	ch <- c.statusEnabled
	ch <- c.statusCacheFull
	ch <- c.statusRestartPending
	ch <- c.statusRestartInProgress
	ch <- c.statusMemoryUsageUsed
	ch <- c.statusMemoryUsageFree
	ch <- c.statusMemoryUsageWasted
	ch <- c.statusMemoryUsageWastedPercent
	ch <- c.statusInternedUsageBufferSize
	ch <- c.statusInternedUsageUsed
	ch <- c.statusInternedUsageFree
	ch <- c.statusInternedUsageNumberOfString
	ch <- c.statusStatisticsNumCachedScript
	ch <- c.statusStatisticsNumCachedKeys
	ch <- c.statusStatisticsMaxCachedKeys
	ch <- c.statusStatisticsHits
	ch <- c.statusStatisticsStartTime
	ch <- c.statusStatisticsLastRestartTime
	ch <- c.statusStatisticsOOMRestart
	ch <- c.statusStatisticsHashRestart
	ch <- c.statusStatisticsManualRestart
	ch <- c.statusStatisticsMisses
	ch <- c.statusStatisticsBlacklistMisses
	ch <- c.statusStatisticsBlacklistMissRatio
	ch <- c.statusStatisticsHitRate
	if c.full {
		ch <- c.statusScriptsHits
		ch <- c.statusScriptsMemoryConsumption
	}
}

func (c opcacheCollector) Collect(ch chan<- prometheus.Metric) {
	var state float64

	if response, err := opcache.GetStatus(c.endpoint, c.timeout); err == nil {
		state = 1

		ch <- prometheus.MustNewConstMetric(c.configEnabled, prometheus.GaugeValue, boolToFloat(response.Configuration.Directives.Enable), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.configEnabledCli, prometheus.GaugeValue, boolToFloat(response.Configuration.Directives.EnableCli), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.configUseCWD, prometheus.GaugeValue, boolToFloat(response.Configuration.Directives.UseCwd), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.configValidateTimestamps, prometheus.GaugeValue, boolToFloat(response.Configuration.Directives.ValidateTimestamps), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.configValidatePermission, prometheus.GaugeValue, boolToFloat(response.Configuration.Directives.ValidatePermission), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.configValidateRoot, prometheus.GaugeValue, boolToFloat(response.Configuration.Directives.ValidateRoot), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.configInheritedHack, prometheus.GaugeValue, boolToFloat(response.Configuration.Directives.InheritedHack), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.configDupsFix, prometheus.GaugeValue, boolToFloat(response.Configuration.Directives.DupsFix), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.configRevalidatePath, prometheus.GaugeValue, boolToFloat(response.Configuration.Directives.RevalidatePath), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.configLogVerbosityLevel, prometheus.GaugeValue, float64(response.Configuration.Directives.LogVerbosityLevel), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.configMemoryConsumption, prometheus.GaugeValue, float64(response.Configuration.Directives.MemoryConsumption), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.configInternedStringsBuffer, prometheus.GaugeValue, float64(response.Configuration.Directives.InternedStringsBuffer), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.configMaxAcceleratedFiles, prometheus.GaugeValue, float64(response.Configuration.Directives.MaxAcceleratedFiles), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.configMaxWastedPercentage, prometheus.GaugeValue, response.Configuration.Directives.MaxWastedPercentage, c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.configConsistencyChecks, prometheus.GaugeValue, float64(response.Configuration.Directives.ConsistencyChecks), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.configForceRestartTimeout, prometheus.GaugeValue, float64(response.Configuration.Directives.ForceRestartTimeout), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.configRevalidateFreq, prometheus.GaugeValue, float64(response.Configuration.Directives.RevalidateFreq), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.configMaxFileSize, prometheus.GaugeValue, float64(response.Configuration.Directives.MaxFileSize), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.configProtectMemory, prometheus.GaugeValue, boolToFloat(response.Configuration.Directives.ProtectMemory), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.configSaveComments, prometheus.GaugeValue, boolToFloat(response.Configuration.Directives.SaveComments), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.configEnableFileOverride, prometheus.GaugeValue, boolToFloat(response.Configuration.Directives.EnableFileOverride), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.configOptimizationLevel, prometheus.GaugeValue, float64(response.Configuration.Directives.OptimizationLevel), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.configFileCacheOnly, prometheus.GaugeValue, boolToFloat(response.Configuration.Directives.FileCacheOnly), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.configFileCacheConsistencyChecks, prometheus.GaugeValue, boolToFloat(response.Configuration.Directives.FileCacheConsistencyChecks), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.configFileUpdateProtection, prometheus.GaugeValue, float64(response.Configuration.Directives.FileUpdateProtection), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.configOptDebugLevel, prometheus.GaugeValue, float64(response.Configuration.Directives.OptDebugLevel), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.configHugeCodePages, prometheus.GaugeValue, boolToFloat(response.Configuration.Directives.HugeCodePages), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.statusEnabled, prometheus.GaugeValue, boolToFloat(response.Status.Enabled), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.statusCacheFull, prometheus.GaugeValue, boolToFloat(response.Status.CacheFull), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.statusRestartPending, prometheus.GaugeValue, boolToFloat(response.Status.RestartPending), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.statusRestartInProgress, prometheus.GaugeValue, boolToFloat(response.Status.RestartInProgress), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.statusMemoryUsageUsed, prometheus.GaugeValue, float64(response.Status.MemoryUsage.UsedMemory), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.statusMemoryUsageFree, prometheus.GaugeValue, float64(response.Status.MemoryUsage.FreeMemory), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.statusMemoryUsageWasted, prometheus.GaugeValue, float64(response.Status.MemoryUsage.WastedMemory), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.statusMemoryUsageWastedPercent, prometheus.GaugeValue, float64(response.Status.MemoryUsage.CurrentWastedPercentage), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.statusInternedUsageBufferSize, prometheus.GaugeValue, float64(response.Status.InternedStringsUsage.BufferSize), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.statusInternedUsageUsed, prometheus.GaugeValue, float64(response.Status.InternedStringsUsage.UsedMemory), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.statusInternedUsageFree, prometheus.GaugeValue, float64(response.Status.InternedStringsUsage.FreeMemory), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.statusInternedUsageNumberOfString, prometheus.GaugeValue, float64(response.Status.InternedStringsUsage.NumberOfStrings), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.statusStatisticsNumCachedScript, prometheus.GaugeValue, float64(response.Status.OpcacheStatistics.NumCachedScripts), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.statusStatisticsNumCachedKeys, prometheus.GaugeValue, float64(response.Status.OpcacheStatistics.MaxCachedKeys), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.statusStatisticsMaxCachedKeys, prometheus.GaugeValue, float64(response.Status.OpcacheStatistics.MaxCachedKeys), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.statusStatisticsHits, prometheus.GaugeValue, float64(response.Status.OpcacheStatistics.Hits), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.statusStatisticsStartTime, prometheus.GaugeValue, float64(response.Status.OpcacheStatistics.StartTime), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.statusStatisticsLastRestartTime, prometheus.GaugeValue, float64(response.Status.OpcacheStatistics.LastRestartTime), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.statusStatisticsOOMRestart, prometheus.GaugeValue, float64(response.Status.OpcacheStatistics.OomRestarts), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.statusStatisticsHashRestart, prometheus.GaugeValue, float64(response.Status.OpcacheStatistics.HashRestarts), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.statusStatisticsManualRestart, prometheus.GaugeValue, float64(response.Status.OpcacheStatistics.ManualRestarts), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.statusStatisticsMisses, prometheus.GaugeValue, float64(response.Status.OpcacheStatistics.Misses), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.statusStatisticsBlacklistMisses, prometheus.GaugeValue, float64(response.Status.OpcacheStatistics.BlacklistMisses), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.statusStatisticsBlacklistMissRatio, prometheus.GaugeValue, float64(response.Status.OpcacheStatistics.BlacklistMissRatio), c.endpoint)
		ch <- prometheus.MustNewConstMetric(c.statusStatisticsHitRate, prometheus.GaugeValue, response.Status.OpcacheStatistics.OpcacheHitRate, c.endpoint)

		if c.full {
			for _, script := range response.Status.Scripts {
				ch <- prometheus.MustNewConstMetric(c.statusScriptsHits, prometheus.GaugeValue, float64(script.Hits), c.endpoint, script.FullPath)
				ch <- prometheus.MustNewConstMetric(c.statusScriptsMemoryConsumption, prometheus.GaugeValue, float64(script.MemoryConsumption), c.endpoint, script.FullPath)
			}
		}
	}

	ch <- prometheus.MustNewConstMetric(c.up, prometheus.GaugeValue, state, c.endpoint)
}

func boolToFloat(v bool) float64 {
	if v {
		return 1
	}

	return 0
}
