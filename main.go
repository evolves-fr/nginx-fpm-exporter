package main

import (
	"fmt"
	"html/template"
	"net/http"
	"os"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	prometheusCollectors "github.com/prometheus/client_golang/prometheus/collectors"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/urfave/cli/v2"
	"gitlab.com/evolves-fr/nginx-fpm-exporter/collectors"
)

var version = "0.0.0-dev"

func main() {
	app := &cli.App{
		Usage:           "Nginx FPM Exporter",
		Description:     "nginx-fpm-exporter exports prometheus compatible metrics from nginx and/or php-fpm.",
		Name:            "nginx-fpm-exporter",
		HelpName:        "nginx-fpm-exporter",
		UsageText:       "nginx-fpm-exporter [global options]",
		Copyright:       "Evolves™",
		Authors:         []*cli.Author{{Name: "Thomas FOURNIER", Email: "tfournier@evolves.fr"}},
		Version:         version,
		Action:          action,
		HideHelpCommand: true,
		Flags: []cli.Flag{
			&cli.BoolFlag{
				Name:    "collector.nginx",
				Usage:   "Enable the nginx collector.",
				EnvVars: []string{"COLLECTOR_NGINX"},
				Value:   true,
			},
			&cli.StringFlag{
				Name:    "collector.nginx.url",
				Usage:   "Nginx stub_status endpoint.",
				EnvVars: []string{"COLLECTOR_NGINX_URL"},
				Value:   "http://127.0.0.1:8080/stub_status",
			},
			&cli.StringSliceFlag{
				Name:    "collector.nginx.labels",
				Usage:   "Nginx metrics constant labels.",
				EnvVars: []string{"COLLECTOR_NGINX_LABELS"},
			},
			&cli.StringFlag{
				Name:    "collector.nginx.timeout",
				Usage:   "Nginx timeout request.",
				EnvVars: []string{"COLLECTOR_NGINX_TIMEOUT"},
				Value:   "2s",
			},
			&cli.BoolFlag{
				Name:    "collector.fpm",
				Usage:   "Enable the PHP FPM collector.",
				EnvVars: []string{"COLLECTOR_FPM"},
				Value:   true,
			},
			&cli.StringFlag{
				Name:    "collector.fpm.url",
				Usage:   "PHP FPM status endpoint.",
				EnvVars: []string{"COLLECTOR_FPM_URL"},
				Value:   "http://127.0.0.1:8080/status",
			},
			&cli.StringSliceFlag{
				Name:    "collector.fpm.labels",
				Usage:   "PHP FPM metrics constant labels.",
				EnvVars: []string{"COLLECTOR_FPM_LABELS"},
			},
			&cli.StringFlag{
				Name:    "collector.fpm.timeout",
				Usage:   "PHP FPM timeout request.",
				EnvVars: []string{"COLLECTOR_FPM_TIMEOUT"},
				Value:   "2s",
			},
			&cli.BoolFlag{
				Name:    "collector.opcache",
				Usage:   "Enable the OPCache collector.",
				EnvVars: []string{"COLLECTOR_OPCACHE"},
				Value:   true,
			},
			&cli.StringFlag{
				Name:    "collector.opcache.url",
				Usage:   "OPCache status endpoint.",
				EnvVars: []string{"COLLECTOR_OPCACHE_URL"},
				Value:   "http://127.0.0.1:8080/opcache",
			},
			&cli.StringSliceFlag{
				Name:    "collector.opcache.labels",
				Usage:   "OPCache metrics constant labels.",
				EnvVars: []string{"COLLECTOR_OPCACHE_LABELS"},
			},
			&cli.StringFlag{
				Name:    "collector.opcache.timeout",
				Usage:   "OPCache timeout request.",
				EnvVars: []string{"COLLECTOR_OPCACHE_TIMEOUT"},
				Value:   "2s",
			},
			&cli.BoolFlag{
				Name: "collector.opcache.full",
				Usage: "Export OPCache scripts metrics",
				EnvVars: []string{"COLLECTOR_OPCACHE_FULL"},
				Value: false,
			},
			&cli.StringFlag{
				Name:    "web.listen-address",
				Usage:   "Address on which to expose metrics and web interface.",
				EnvVars: []string{"WEB_LISTEN_ADDRESS"},
				Value:   ":9868",
			},
			&cli.StringFlag{
				Name:    "web.telemetry-path",
				Usage:   "Path under which to expose metrics.",
				EnvVars: []string{"WEB_TELEMETRY_PATH"},
				Value:   "/metrics",
			},
			&cli.BoolFlag{
				Name:    "web.disable-exporter-metrics",
				Usage:   "Exclude metrics about the exporter itself (promhttp_*, process_*, go_*).",
				EnvVars: []string{"WEB_DISABLE_EXPORTER_METRICS"},
				Value:   false,
			},
		},
	}

	if err := app.Run(os.Args); err != nil {
		panic(err)
	}
}

func action(c *cli.Context) error {
	var (
		address       = c.String("web.listen-address")
		telemetryPath = c.String("web.telemetry-path")
	)

	r := prometheus.NewRegistry()

	// Nginx collector
	if c.Bool("collector.nginx") {
		timeout, err := time.ParseDuration(c.String("collector.nginx.timeout"))
		if err != nil {
			return err
		}

		r.MustRegister(collectors.NewNginxCollector(
			c.String("collector.nginx.url"),
			timeout,
			c.StringSlice("collector.nginx.labels"),
		))
	}

	// PHP FPM collector
	if c.Bool("collector.fpm") {
		timeout, err := time.ParseDuration(c.String("collector.fpm.timeout"))
		if err != nil {
			return err
		}

		r.MustRegister(collectors.NewFPMCollector(
			c.String("collector.fpm.url"),
			timeout,
			c.StringSlice("collector.fpm.labels"),
		))
	}

	// OPCache collector
	if c.Bool("collector.opcache") {
		timeout, err := time.ParseDuration(c.String("collector.opcache.timeout"))
		if err != nil {
			return err
		}

		r.MustRegister(collectors.NewOPCacheCollector(
			c.String("collector.opcache.url"),
			timeout,
			c.StringSlice("collector.opcache.labels"),
			c.Bool("collector.opcache.full"),
		))
	}

	// Default Go collector
	if !c.Bool("web.disable-exporter-metrics") {
		r.MustRegister(prometheusCollectors.NewGoCollector())
	}

	// Prometheus handler
	http.Handle(telemetryPath, promhttp.HandlerFor(r, promhttp.HandlerOpts{}))

	// Index handler
	if telemetryPath != "/" {
		http.HandleFunc("/", indexHandler(telemetryPath))
	}

	fmt.Printf("Starting %s %s on %s\n", c.App.Name, c.App.Version, address)

	return http.ListenAndServe(address, nil)
}

func indexHandler(telemetryPath string) http.HandlerFunc {
	var indexTemplate = `<html lang="en">
<head>
    <title>Nginx FPM Exporter</title>
</head>
<body>
<h1>Nginx FPM Exporter - <small>{{ .Version }}</small></h1>
<ul>
    <li><a href="{{ .TelemetryPath }}">{{ .TelemetryPath }}</a></li>
</ul>
</body>
</html>`

	data := map[string]string{
		"Version":       version,
		"TelemetryPath": telemetryPath,
	}

	return func(w http.ResponseWriter, r *http.Request) {
		if err := template.Must(template.New("index.html").Parse(indexTemplate)).Execute(w, data); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write([]byte(err.Error()))
		}
	}
}
