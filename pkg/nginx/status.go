package nginx

import (
	"crypto/tls"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"reflect"
	"regexp"
	"strconv"
	"time"
)

func GetStatus(statusURL string, timeout time.Duration) (*Status, error) {
	httpClient := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				MinVersion: tls.VersionTLS12,
			},
			TLSHandshakeTimeout:   timeout,
			IdleConnTimeout:       timeout,
			ResponseHeaderTimeout: timeout,
			ExpectContinueTimeout: timeout,
		},
		Timeout: timeout,
	}

	baseURL, err := url.Parse(statusURL)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest(http.MethodGet, baseURL.String(), nil)
	if err != nil {
		return nil, err
	}

	res, err := httpClient.Do(req)
	if err != nil {
		return nil, err
	}

	if res.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("%s", res.Status)
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	if err = res.Body.Close(); err != nil {
		return nil, err
	}

	status := Status{}

	if err := status.Unmarshal(body); err != nil {
		return nil, err
	}

	return &status, nil
}

type Status struct {
	ActiveConnections int `json:"active_connections" regex:"active_connections"`
	Accepts           int `json:"accepts" regex:"accepts"`
	Handled           int `json:"handled" regex:"handled"`
	Requests          int `json:"requests" regex:"requests"`
	Reading           int `json:"reading" regex:"reading"`
	Writing           int `json:"writing" regex:"writing"`
	Waiting           int `json:"waiting" regex:"waiting"`
}

func (s *Status) Unmarshal(b []byte) error {
	var rxp = regexp.MustCompile(`(?m)^Active connections: (?P<active_connections>\d+) 
server accepts handled requests
 (?P<accepts>\d+) (?P<handled>\d+) (?P<requests>\d+) 
Reading: (?P<reading>\d+) Writing: (?P<writing>\d+) Waiting: (?P<waiting>\d+) $`)

	var (
		typeOf  = reflect.TypeOf(*s)
		valueOf = reflect.ValueOf(s)
	)

	match := rxp.FindStringSubmatch(string(b))

	for i, name := range rxp.SubexpNames() {
		if i != 0 && name != "" {
			v, err := strconv.Atoi(match[i])
			if err != nil {
				return err
			}

			for i := 0; i < typeOf.NumField(); i++ {
				if name == typeOf.Field(i).Tag.Get("regex") {
					valueOf.Elem().FieldByName(typeOf.Field(i).Name).SetInt(int64(v))
				}
			}
		}
	}

	return nil
}
