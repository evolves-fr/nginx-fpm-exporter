package opcache

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"net/http"
	"time"
)

func GetStatus(statusURL string, timeout time.Duration) (*Response, error) {
	httpClient := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				MinVersion: tls.VersionTLS12,
			},
			TLSHandshakeTimeout:   timeout,
			IdleConnTimeout:       timeout,
			ResponseHeaderTimeout: timeout,
			ExpectContinueTimeout: timeout,
		},
		Timeout: timeout,
	}

	req, err := http.NewRequest(http.MethodGet, statusURL, nil)
	if err != nil {
		return nil, err
	}

	res, err := httpClient.Do(req)
	if err != nil {
		return nil, err
	}

	if res.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("%s", res.Status)
	}

	response := Response{}

	if err = json.NewDecoder(res.Body).Decode(&response); err != nil {
		return nil, err
	}

	if err = res.Body.Close(); err != nil {
		return nil, err
	}

	return &response, nil
}

type (
	Response struct {
		Configuration Configuration `json:"configuration"`
		Status        Status        `json:"status"`
	}

	Configuration struct {
		Directives Directives    `json:"directives"`
		Version    Version       `json:"version"`
		Blacklist  []interface{} `json:"blacklist"`
	}

	Directives struct {
		PreferredMemoryModel       string  `json:"opcache.preferred_memory_model"`
		BlacklistFilename          string  `json:"opcache.blacklist_filename"`
		ErrorLog                   string  `json:"opcache.error_log"`
		LockfilePath               string  `json:"opcache.lockfile_path"`
		FileCache                  string  `json:"opcache.file_cache"`
		RestrictAPI                string  `json:"opcache.restrict_api"`
		MaxWastedPercentage        float64 `json:"opcache.max_wasted_percentage"`
		LogVerbosityLevel          int     `json:"opcache.log_verbosity_level"`
		MemoryConsumption          int     `json:"opcache.memory_consumption"`
		InternedStringsBuffer      int     `json:"opcache.interned_strings_buffer"`
		MaxAcceleratedFiles        int     `json:"opcache.max_accelerated_files"`
		ConsistencyChecks          int     `json:"opcache.consistency_checks"`
		ForceRestartTimeout        int     `json:"opcache.force_restart_timeout"`
		RevalidateFreq             int     `json:"opcache.revalidate_freq"`
		MaxFileSize                int     `json:"opcache.max_file_size"`
		OptimizationLevel          int     `json:"opcache.optimization_level"`
		FileUpdateProtection       int     `json:"opcache.file_update_protection"`
		OptDebugLevel              int     `json:"opcache.opt_debug_level"`
		Enable                     bool    `json:"opcache.enable"`
		EnableCli                  bool    `json:"opcache.enable_cli"`
		UseCwd                     bool    `json:"opcache.use_cwd"`
		ValidateTimestamps         bool    `json:"opcache.validate_timestamps"`
		ValidatePermission         bool    `json:"opcache.validate_permission"`
		ValidateRoot               bool    `json:"opcache.validate_root"`
		InheritedHack              bool    `json:"opcache.inherited_hack"`
		DupsFix                    bool    `json:"opcache.dups_fix"`
		RevalidatePath             bool    `json:"opcache.revalidate_path"`
		ProtectMemory              bool    `json:"opcache.protect_memory"`
		SaveComments               bool    `json:"opcache.save_comments"`
		EnableFileOverride         bool    `json:"opcache.enable_file_override"`
		FileCacheOnly              bool    `json:"opcache.file_cache_only"`
		FileCacheConsistencyChecks bool    `json:"opcache.file_cache_consistency_checks"`
		HugeCodePages              bool    `json:"opcache.huge_code_pages"`
	}

	Version struct {
		Version     string `json:"version"`
		ProductName string `json:"opcache_product_name"`
	}

	Status struct {
		Enabled              bool                 `json:"opcache_enabled"`
		CacheFull            bool                 `json:"cache_full"`
		RestartPending       bool                 `json:"restart_pending"`
		RestartInProgress    bool                 `json:"restart_in_progress"`
		MemoryUsage          MemoryUsage          `json:"memory_usage"`
		InternedStringsUsage InternedStringsUsage `json:"interned_strings_usage"`
		OpcacheStatistics    Statistics           `json:"opcache_statistics"`
		Scripts              map[string]Script    `json:"scripts"`
	}

	MemoryUsage struct {
		UsedMemory              int `json:"used_memory"`
		FreeMemory              int `json:"free_memory"`
		WastedMemory            int `json:"wasted_memory"`
		CurrentWastedPercentage int `json:"current_wasted_percentage"`
	}

	InternedStringsUsage struct {
		BufferSize      int `json:"buffer_size"`
		UsedMemory      int `json:"used_memory"`
		FreeMemory      int `json:"free_memory"`
		NumberOfStrings int `json:"number_of_strings"`
	}

	Statistics struct {
		NumCachedScripts   int     `json:"num_cached_scripts"`
		NumCachedKeys      int     `json:"num_cached_keys"`
		MaxCachedKeys      int     `json:"max_cached_keys"`
		Hits               int     `json:"hits"`
		StartTime          int     `json:"start_time"`
		LastRestartTime    int     `json:"last_restart_time"`
		OomRestarts        int     `json:"oom_restarts"`
		HashRestarts       int     `json:"hash_restarts"`
		ManualRestarts     int     `json:"manual_restarts"`
		Misses             int     `json:"misses"`
		BlacklistMisses    int     `json:"blacklist_misses"`
		BlacklistMissRatio int     `json:"blacklist_miss_ratio"`
		OpcacheHitRate     float64 `json:"opcache_hit_rate"`
	}

	Script struct {
		FullPath          string `json:"full_path"`
		Hits              int    `json:"hits"`
		MemoryConsumption int    `json:"memory_consumption"`
		LastUsed          string `json:"last_used"`
		LastUsedTimestamp int    `json:"last_used_timestamp"`
	}
)
