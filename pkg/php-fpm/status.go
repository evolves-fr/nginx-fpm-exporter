package phpfpm

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"time"
)

const (
	ProcessManagerStatic   = "static"
	ProcessManagerDynamic  = "dynamic"
	ProcessManagerOnDemand = "ondemand"
)

func GetStatus(statusURL string, timeout time.Duration) (*Status, error) {
	httpClient := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				MinVersion: tls.VersionTLS12,
			},
			TLSHandshakeTimeout:   timeout,
			IdleConnTimeout:       timeout,
			ResponseHeaderTimeout: timeout,
			ExpectContinueTimeout: timeout,
		},
		Timeout: timeout,
	}

	baseURL, err := url.Parse(statusURL)
	if err != nil {
		return nil, err
	}

	baseURL.RawQuery = "full&json"

	req, err := http.NewRequest(http.MethodGet, baseURL.String(), nil)
	if err != nil {
		return nil, err
	}

	res, err := httpClient.Do(req)
	if err != nil {
		return nil, err
	}

	if res.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("%s", res.Status)
	}

	status := Status{}

	if err = json.NewDecoder(res.Body).Decode(&status); err != nil {
		return nil, err
	}

	if err = res.Body.Close(); err != nil {
		return nil, err
	}

	return &status, nil
}

type (
	Status struct {
		Pool               string    `json:"pool"`
		ProcessManager     string    `json:"process manager"`
		StartTime          int       `json:"start time"`
		StartSince         int       `json:"start since"`
		AcceptedConn       int       `json:"accepted conn"`
		ListenQueue        int       `json:"listen queue"`
		MaxListenQueue     int       `json:"max listen queue"`
		ListenQueueLen     int       `json:"listen queue len"`
		IdleProcesses      int       `json:"idle processes"`
		ActiveProcesses    int       `json:"active processes"`
		TotalProcesses     int       `json:"total processes"`
		MaxActiveProcesses int       `json:"max active processes"`
		MaxChildrenReached int       `json:"max children reached"`
		SlowRequests       int       `json:"slow requests"`
		Processes          []Process `json:"processes"`
	}

	Process struct {
		Pid               int     `json:"pid"`
		State             string  `json:"state"`
		StartTime         int     `json:"start time"`
		StartSince        int     `json:"start since"`
		Requests          int     `json:"requests"`
		RequestDuration   int     `json:"request duration"`
		RequestMethod     string  `json:"request method"`
		RequestURI        string  `json:"request uri"`
		ContentLength     int     `json:"content length"`
		User              string  `json:"user"`
		Script            string  `json:"script"`
		LastRequestCPU    float64 `json:"last request cpu"`
		LastRequestMemory int     `json:"last request memory"`
	}
)
