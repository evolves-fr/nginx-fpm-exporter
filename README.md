# Nginx FPM Exporter

Prometheus exporter for Nginx and PHP-FPM metrics, written in Go with pluggable metric collectors.

## Notes
- Use Nginx for PHP-FPM status.

## Usages
```
NAME:
   nginx-fpm-exporter - Nginx FPM Exporter

USAGE:
   nginx-fpm-exporter [global options]

VERSION:
   v1.3.0

DESCRIPTION:
   nginx-fpm-exporter exports prometheus compatible metrics from nginx and/or php-fpm.

AUTHOR:
   Thomas FOURNIER <tfournier@evolves.fr>

GLOBAL OPTIONS:
   --collector.nginx                  Enable the nginx collector. (default: true) [$COLLECTOR_NGINX]
   --collector.nginx.url value        Nginx stub_status endpoint. (default: "http://127.0.0.1:8080/stub_status") [$COLLECTOR_NGINX_URL]
   --collector.nginx.labels value     Nginx metrics constant labels. [$COLLECTOR_NGINX_LABELS]
   --collector.nginx.timeout value    Nginx timeout request. (default: "2s") [$COLLECTOR_NGINX_TIMEOUT]
   --collector.fpm                    Enable the PHP FPM collector. (default: true) [$COLLECTOR_FPM]
   --collector.fpm.url value          PHP FPM status endpoint. (default: "http://127.0.0.1:8080/status") [$COLLECTOR_FPM_URL]
   --collector.fpm.labels value       PHP FPM metrics constant labels. [$COLLECTOR_FPM_LABELS]
   --collector.fpm.timeout value      PHP FPM timeout request. (default: "2s") [$COLLECTOR_FPM_TIMEOUT]
   --collector.opcache                Enable the OPCache collector. (default: true) [$COLLECTOR_OPCACHE]
   --collector.opcache.url value      OPCache status endpoint. (default: "http://127.0.0.1:8080/opcache") [$COLLECTOR_OPCACHE_URL]
   --collector.opcache.labels value   OPCache metrics constant labels. [$COLLECTOR_OPCACHE_LABELS]
   --collector.opcache.timeout value  OPCache timeout request. (default: "2s") [$COLLECTOR_OPCACHE_TIMEOUT]
   --web.listen-address value         Address on which to expose metrics and web interface. (default: ":9868") [$WEB_LISTEN_ADDRESS]
   --web.telemetry-path value         Path under which to expose metrics. (default: "/metrics") [$WEB_TELEMETRY_PATH]
   --web.disable-exporter-metrics     Exclude metrics about the exporter itself (promhttp_*, process_*, go_*). (default: false) [$WEB_DISABLE_EXPORTER_METRICS]
   --help, -h                         show help (default: false)
   --version, -v                      print the version (default: false)

COPYRIGHT:
   Evolves™
```

## Nginx and PHP-FPM configuration

### Nginx

Add into server configuration :

```nginx configuration
server {
    listen                      80 default_server;
    listen                      [::]:80 default_server;
    
    # [...]

    # Nginx status page
    location ~ ^/stub_status {
        access_log                      off;
        allow                           127.0.0.1:
        deny                            all;
        stub_status;
    }

    # PHP-FPM status page
    location ~ ^/status$ {
        access_log                      off;
        allow                           127.0.0.1:
        deny                            all;
        include                         fastcgi_params;
        fastcgi_pass                    unix:/run/php-fpm/www.sock;
        fastcgi_index                   index.php;
        fastcgi_param DOCUMENT_ROOT     $realpath_root;
        fastcgi_param SCRIPT_NAME       $fastcgi_script_name;
        fastcgi_param SCRIPT_FILENAME   $realpath_root$fastcgi_script_name;
    }
    
    # OPCache status
    location ~ ^/opcache {
        access_log                      off;
        allow                           127.0.0.1;
        allow                           10.197.0.0/16;
        allow                           100.64.0.0/16;
        deny                            all;
        include                         fastcgi_params;
        fastcgi_pass                    unix:/run/php-fpm/www.sock;
        fastcgi_index                   opcache.php;
        fastcgi_param DOCUMENT_ROOT     /usr/local/share/php;
        fastcgi_param SCRIPT_NAME       opcache.php;
        fastcgi_param SCRIPT_FILENAME   /usr/local/share/php/opcache.php;
    }
}
```

### PHP-FPM

Uncomment `pm.status_path = /status`

### OPCache

```php
<?php echo json_encode(array("configuration" => opcache_get_configuration(), "status" => opcache_get_status()), JSON_UNESCAPED_SLASHES); ?>
```
