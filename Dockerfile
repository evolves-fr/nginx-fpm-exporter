FROM scratch

EXPOSE      9868
ENTRYPOINT  ["/nginx-fpm-exporter"]

COPY nginx-fpm-exporter /
